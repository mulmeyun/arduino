// following this amazing tutorial
// https://howtomechatronics.com/tutorials/arduino/how-to-build-an-arduino-wireless-network-with-multiple-nrf24l01-modules/

#include <SPI.h>
#include <RF24.h>
#include <RF24Network.h>

RF24 radio(48, 49);              // nRF24L01 (CE,CSN)
RF24Network network(radio);      // Include the radio in the network
const uint16_t this_node = 00;   // Address of our node in Octal format ( 04,031, etc)

byte incomingData[2];
int data[10];

#include <Servo.h>
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo servo5;
Servo servo6;
Servo servo7;
Servo servo8;
Servo servo9;
Servo servo10;

void setup() {
  Serial.begin(9600);
  SPI.begin();

  pinMode(53, OUTPUT);
//  On the Arduino Duemilanove and other ATmega168 / 328-based boards,
//  the SPI bus uses pins 10 (SS), 11 (MOSI), 12 (MISO), and 13 (SCK).
//  On the Arduino Mega, this is 50 (MISO), 51 (MOSI), 52 (SCK), and 53 (SS).
//  Note that even if you're not using the SS pin, it must remain set as an output;
//  otherwise, the SPI interface can be put into slave mode, rendering the library inoperative.
  
  radio.begin();
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_250KBPS);
  network.begin(108, this_node); //(channel 2400 + 0-125, node address)

  servo1.attach(23);
  servo2.attach(25);
  servo3.attach(27);
  servo4.attach(29);
  servo5.attach(31);
  servo6.attach(33);
  servo7.attach(35);
  servo8.attach(37);
  servo9.attach(39);
  servo10.attach(41);
}

void loop() {
  network.update();
  while ( network.available() ) {     // Is there any incoming data?
    RF24NetworkHeader header;
    network.read(header, &incomingData, sizeof(incomingData)); // Read the incoming data

    // route data from different nodes
    switch (header.from_node) {
      case 01: data[0] = incomingData[0]; data[1] = incomingData[1]; break;
      case 02: data[2] = incomingData[0]; data[3] = incomingData[1]; break;
      case 03: data[4] = incomingData[0]; data[5] = incomingData[1]; break;
      case 04: data[6] = incomingData[0]; data[7] = incomingData[1]; break;
      case 05: data[8] = incomingData[0]; data[9] = incomingData[1]; break;
    }

    for (byte i=0; i<10; i++){
      Serial.print(data[i]);
      Serial.print(" ");
    }
    Serial.println();

    servo1.write(map(data[0], 0, 300, 0, 180));
    servo2.write(map(data[1], 0, 300, 0, 180));
    servo3.write(map(data[2], 0, 300, 0, 180));
    servo4.write(map(data[3], 0, 300, 0, 180));
    servo5.write(map(data[4], 0, 300, 0, 180));
    servo6.write(map(data[5], 0, 300, 0, 180));
    servo7.write(map(data[6], 0, 300, 0, 180));
    servo8.write(map(data[7], 0, 300, 0, 180));
    servo9.write(map(data[8], 0, 300, 0, 180));
    servo10.write(map(data[9], 0, 300, 0, 180));
  }
}
