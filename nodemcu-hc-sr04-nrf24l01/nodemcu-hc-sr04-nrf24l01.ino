//nodemcu pins
//    D0 = 16
//    D1 = 5
//    D2 = 4
//    D3 = 0
//    D4 = 2

//    D5 = 14
//    D6 = 12
//    D7 = 13
//    D8 = 15

//    RX = 3
//    TX = 1

// remember to use a voltage divider on the echo pin of the hc-sr04

#include <NewPing.h>

const int maxDistance = 300;

//trigger, echo, dist
NewPing sonar1(16, 5, maxDistance); // NewPing setup of pins and maximum distance.
NewPing sonar2(0,15, maxDistance);

int dist1 = 0;
int dist2 = 0;

#include <RF24.h>
#include <RF24Network.h>
#include <SPI.h>

RF24 radio(2, 4);               // nRF24L01 (CE,CSN)
RF24Network network(radio);      // Include the radio in the network
const uint16_t this_node = 05;   // Address of this node in Octal format ( 04,031, etc)
const uint16_t mega = 00;      

void setup() {
  Serial.begin(9600); // Open serial monitor at 115200 baud to see ping results.

  //BLINK
  pinMode(LED_BUILTIN, OUTPUT);

  SPI.begin();
  radio.begin();
  network.begin(90, this_node);
}

void loop() {
  digitalWrite(LED_BUILTIN, LOW);
  
//  Serial.print(sonar1.ping_cm());
//  Serial.print(" ");
//
//  Serial.print(sonar2.ping_cm());
//  Serial.println();

  dist1 = sonar1.ping_cm();
  dist2 = sonar2.ping_cm();

  network.update();
  RF24NetworkHeader header(mega);     // (Address where the data is going)
  bool ok = network.write(header, &dist1, sizeof(dist1)); // Send the data

  delay(50);

  //digitalWrite(LED_BUILTIN, HIGH);
}
