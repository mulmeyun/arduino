/*
  Arduino Wireless Network - Multiple NRF24L01 Tutorial
  == Example 01 - Servo Control / Node 01 - Servo motor ==
*/
#include <RF24.h>
#include <RF24Network.h>
#include <SPI.h>

RF24 radio(48, 49);              // nRF24L01 (CE,CSN)
RF24Network network(radio);      // Include the radio in the network
const uint16_t this_node = 00;   // Address of our node in Octal format ( 04,031, etc)

void setup() {
  Serial.begin(9600);

  pinMode(53, OUTPUT);
//  On the Arduino Duemilanove and other ATmega168 / 328-based boards,
//  the SPI bus uses pins 10 (SS), 11 (MOSI), 12 (MISO), and 13 (SCK).
//  On the Arduino Mega, this is 50 (MISO), 51 (MOSI), 52 (SCK), and 53 (SS).
//  Note that even if you're not using the SS pin, it must remain set as an output;
//  otherwise, the SPI interface can be put into slave mode, rendering the library inoperative.
  
  SPI.begin();
  radio.begin();
  network.begin(90, this_node); //(channel, node address)
}

void loop() {
  network.update();
  while ( network.available() ) {     // Is there any incoming data?
    RF24NetworkHeader header;
    unsigned long incomingData;
    network.read(header, &incomingData, sizeof(incomingData)); // Read the incoming data

    //Serial.print(incomingData);
    //Serial.println();

    // this is terrible but i can't be bothered
    if (header.from_node == 01) {
      //Serial.print("from node 01 ");
      Serial.print(incomingData);
      Serial.print(" ");
    }
    if (header.from_node == 02) {
      //Serial.print("from node 02 ");
      Serial.print(incomingData);
      Serial.print(" ");
    }
    if (header.from_node == 03) {
      //Serial.print("from node 03 ");
      Serial.print(incomingData);
      Serial.print(" ");
    }
    if (header.from_node == 04) {
      //Serial.print("from node 04 ");
      Serial.print(incomingData);
      Serial.print(" ");
    }
    if (header.from_node == 05) {
      //Serial.print("from node 05 ");
      Serial.print(incomingData);
      Serial.print(" ");
    }

    Serial.println();
  }
}
