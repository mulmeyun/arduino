#include <NewPing.h>

const int maxDistance = 300;
const int trig1 = 8;
const int echo1 = 8;

NewPing sonar(trig1, echo1, maxDistance);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(50);
  Serial.print("Ping: ");
  Serial.print(sonar.ping_cm());
  Serial.println("cm");
}
