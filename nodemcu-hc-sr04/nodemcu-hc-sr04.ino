//nodemcu pins
//    D0 = 16
//    D1 = 5
//    D2 = 4
//    D3 = 0
//    D4 = 2

//    D5 = 14
//    D6 = 12
//    D7 = 13
//    D8 = 15

//    RX = 3
//    TX = 1

// remember to use a voltage divider on the echo pin of the hc-sr04

#include <NewPing.h>

const int maxDistance = 300;

//trigger, echo, dist
NewPing sonar1(14, 12, maxDistance); // NewPing setup of pins and maximum distance.
NewPing sonar2(13,15, maxDistance);

#include <RF24.h>

void setup() {
  Serial.begin(9600); // Open serial monitor at 115200 baud to see ping results.

  //BLINK
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  digitalWrite(LED_BUILTIN, LOW);
  
  Serial.print(sonar1.ping_cm());
  Serial.print(" ");

  Serial.print(sonar2.ping_cm());
  Serial.println();

  delay(50);

  //digitalWrite(LED_BUILTIN, HIGH);
}
