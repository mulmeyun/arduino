#include <Servo.h>
#include <NewPing.h>

const int servoPin1 = 2;
const int servoPin2 = 3;
const int servoPin3 = 4;
const int servoPin4 = 5;
const int servoPin5 = 6;

int servoAngle1 = 0;   // servo position in degrees
int servoAngle2 = 0;
int servoAngle3 = 0;
int servoAngle4 = 0;
int servoAngle5 = 0;

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo servo5;

const int maxDistance = 450;
const int sonarInterval = 29;

const int sonarPin1 = 22;
const int sonarPin2 = 23;
const int sonarPin3 = 24;
const int sonarPin4 = 25;
const int sonarPin5 = 26;

int sonar1Distance = 0;
int sonar2Distance = 0;
int sonar3Distance = 0;
int sonar4Distance = 0;
int sonar5Distance = 0;

NewPing sonar1(sonarPin1, sonarPin1, maxDistance);

unsigned long sonarPreviousMillis = 0;

unsigned long clockPreviousMillis = 0;
const int clockInterval = 1000;

void setup()
{
  Serial.begin(9600);
  servo1.attach(servoPin1);
  servo2.attach(servoPin2);
  servo3.attach(servoPin3);
  servo4.attach(servoPin4);
  servo5.attach(servoPin5);
}

void loop()
{
  unsigned long sonarCurrentMillis = millis();
  if (sonarCurrentMillis - sonarPreviousMillis >= sonarInterval) {
    sonarPreviousMillis = sonarCurrentMillis;

    sonar1Distance = sonar1.ping_cm();

    Serial.print(sonar1Distance);
    Serial.println();

    servo1.write(sonar1Distance);
    servo3.write(sonar1Distance);
    servo4.write(sonar1Distance);
    servo5.write(sonar1Distance);
  }

  unsigned long clockCurrentMillis = millis();
  if (clockCurrentMillis - clockPreviousMillis >= clockInterval) {
    clockPreviousMillis = clockCurrentMillis;

    if (servoAngle2 == 60) {
      servoAngle2 = 120;
    } else {
      servoAngle2 = 60;
    }

    servo2.write(servoAngle2);
  }
}
