#include <NewPing.h>
#include <Servo.h>

const int sonarMaxDistance = 300;

NewPing sonar1(22, 22, sonarMaxDistance);
NewPing sonar2(23, 23, sonarMaxDistance);
NewPing sonar3(24, 24, sonarMaxDistance);
NewPing sonar4(25, 25, sonarMaxDistance);
NewPing sonar5(26, 26, sonarMaxDistance);

//const int sonarNum = 5;
//NewPing sonar[sonarNum] = {
//  NewPing(22, 22, sonarMaxDist),
//  NewPing(23, 23, sonarMaxDist),
//  NewPing(24, 24, sonarMaxDist),
//  NewPing(25, 25, sonarMaxDist),
//  NewPing(26, 26, sonarMaxDist)
//};

const int sonarInterval = 29;
unsigned long sonarPreviousTime = 0;

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo servo5;

void setup() {
  Serial.begin(115200);
  servo1.attach(2);
  servo2.attach(3);
  servo3.attach(4);
  servo4.attach(5);
  servo5.attach(6);
}

void loop() {
  unsigned long currentTime = millis();

  if (currentTime - sonarPreviousTime >= sonarInterval) {
    sonarPreviousTime = currentTime;

//    servo1.write(sonar1.ping_cm());
//    servo2.write(sonar1.ping_cm());
//    servo3.write(sonar1.ping_cm());
//    servo4.write(sonar1.ping_cm());
//    servo5.write(sonar1.ping_cm());

    servo1.write(map(sonar1.ping_cm(), 0, 300, 0, 180));
    servo2.write(map(sonar2.ping_cm(), 0, 300, 0, 180));
    servo3.write(map(sonar3.ping_cm(), 0, 300, 0, 180));
    servo4.write(map(sonar4.ping_cm(), 0, 300, 0, 180));
    servo5.write(map(sonar5.ping_cm(), 0, 300, 0, 180));
    
//    for (int i = 0; i < sonarNum; i++) {
//      Serial.print(sonar[i].ping_cm());
//      Serial.print(" ");
//    }
//    Serial.println();
  }
}
