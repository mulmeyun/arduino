#include <Servo.h>
 
int const servoPin = 8;
int const servoPin2 = 7;
int servoAngle = 0;   // servo position in degrees
int servoAngle2 = 0;
int del = 100;

int const analogPin = 0;
int val = 0;

Servo servo;  
Servo servo2;
 
void setup()
{
  Serial.begin(9600);  
  servo.attach(servoPin);
  servo2.attach(servoPin2);
}
  
void loop()
{
  val = analogRead(analogPin);
  val = map(val, 0, 1023, 180, 0);
  //Serial.println(val);
  servo.write(val);
  delay(del);

  delay(500);
  servo2.write(0);
  delay(500);
  servo2.write(130);

//  del = del - 100;

//  if (del == 100)
//  {
//    del = 1000;
//  }
//  for(servoAngle = 30; servoAngle < 60; servoAngle++)  //move the micro servo from 0 degrees to 180 degrees
//  {                                  
//    servo.write(servoAngle);              
//    delay(5);                  
//  }
//
//  delay(1000);
//
//  for(servoAngle = 60; servoAngle > 30; servoAngle--)  //now move back the micro servo from 0 degrees to 180 degrees
//  {                                
//    servo.write(servoAngle);          
//    delay(5);      
//  }
}
