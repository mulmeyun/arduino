#include <Servo.h>
 
const int servoPin1 = 8;
const int servoPin2 = 9;
const int servoPin3 = 10;
const int servoPin4 = 11;
const int servoPin5 = 12;

int servoAngle1 = 0;   // servo position in degrees
int servoAngle2 = 0;
int servoAngle3 = 0;
int servoAngle4 = 0;
int servoAngle5 = 0;

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo servo5;

const int analogPin = 0;
int analogVal = 0;

unsigned long previousMillis = 0;
const int interval = 1000;
 
void setup()
{
  Serial.begin(9600);  
  servo1.attach(servoPin1);
  servo2.attach(servoPin2);
  servo3.attach(servoPin3);
  servo4.attach(servoPin4);
  servo5.attach(servoPin5);
}
  
void loop()
{
  analogVal = analogRead(analogPin);
  analogVal = map(analogVal, 0, 1023, 180, 0);
  //Serial.println(analogVal);

  servo1.write(analogVal);
  
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval){
    previousMillis = currentMillis;

    if (servoAngle2 == 60) {
      servoAngle2 = 120;
    } else {
      servoAngle2 = 60;
    }

    servo2.write(servoAngle2);
  }
}
