#include <NewPing.h>

const int sonarMaxDistance = 300;

NewPing sonar1(22, 23, sonarMaxDistance);
NewPing sonar2(24, 25, sonarMaxDistance);
//NewPing sonar3(24, 24, sonarMaxDistance);
//NewPing sonar4(25, 25, sonarMaxDistance);

const int sonarInterval = 29;
unsigned long sonarPreviousTime = 0;

int dist1 = 0;
int dist2 = 0;
//int dist3 = 0;
//int dist4 = 0;

const int buzzer = 8;
int del1 = 0;
int del2 = 0;

void setup() {
  Serial.begin(115200);

  pinMode(buzzer, OUTPUT);
}

void loop() {
  unsigned long currentTime = millis();

  if (currentTime - sonarPreviousTime >= sonarInterval) {
    sonarPreviousTime = currentTime;

    dist1 = (sonar1.ping_cm());
    dist2 = (sonar2.ping_cm());
    //dist3 = (sonar3.ping_cm());
    //dist4 = (sonar4.ping_cm());
  }
  
  Serial.print(dist1);
  Serial.print(" ");
  Serial.print(dist2);
  Serial.println();
  
  del1 = map(dist1, 0, 300, 1, 100);
  del2 = map(dist2, 0, 300, 1, 100);

  digitalWrite(buzzer, HIGH);
  delay(del1);
  digitalWrite(buzzer, LOW);
  delay(del2);
}
