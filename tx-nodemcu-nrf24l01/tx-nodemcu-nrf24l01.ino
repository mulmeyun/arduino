//nodemcu pins
//    D0 = 16
//    D1 = 5
//    D2 = 4
//    D3 = 0
//    D4 = 2

//    D5 = 14
//    D6 = 12
//    D7 = 13
//    D8 = 15

//    RX = 3
//    TX = 1

// remember to use a voltage divider on the echo pin of the hc-sr04

#include <NewPing.h>

const int maxDistance = 300;

// trigger, echo, dist
NewPing sonar1(16, 5, maxDistance);
NewPing sonar2(0,15, maxDistance);

byte data[2];

#include <SPI.h>
#include <RF24.h>
#include <RF24Network.h>

RF24 radio(2, 4);               // nRF24L01 (CE,CSN)
RF24Network network(radio);      // Include the radio in the network
const uint16_t this_node = 01;   // Address of this node in Octal format ( 04,031, etc)
const uint16_t base = 00;      

void setup() {
  Serial.begin(9600); // Open serial monitor at 115200 baud to see ping results.
  SPI.begin();
  
  //BLINK
  pinMode(LED_BUILTIN, OUTPUT);
  
  radio.begin();
  radio.setPALevel(RF24_PA_MIN);
  radio.setDataRate(RF24_250KBPS);
  network.begin(108, this_node);    //(channel 2400 + 0-125, node address)
}

void loop() {
  digitalWrite(LED_BUILTIN, LOW);
  
  data[0] = sonar1.ping_cm();
  data[1] = sonar2.ping_cm();

  Serial.print(data[0]);
  Serial.print(" ");
  Serial.print(data[1]);
  Serial.println();

  network.update();
  RF24NetworkHeader header(base);     // (Address where the data is going)
  bool ok = network.write(header, &data, sizeof(data)); // Send the data

  delay(29);

  //digitalWrite(LED_BUILTIN, HIGH);
}
